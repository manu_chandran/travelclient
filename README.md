# TravelClient

This is a MEAN Stack project demonstrating the prototype functionality of a Immigration application.The whole applicaton comprises 
of two modules, the client and the server and this module contains the client side functionality.This project was generated 
with [Angular CLI](https://github.com/angular/angular-cli) version 1.4.9.

## Getting Started

Please clone the project using the command - git clone https://manu_chandran@bitbucket.org/manu_chandran/travelclient.git

To execute the related server side functionality, clone the dependent project - git clone https://manu_chandran@bitbucket.org/manu_chandran/travelservice.git


### Prerequisites

Node JS should be installed for executing this application.



### Installing

1. Clone the project to the local workspace

2. Navigate to the project base folder and execute the command - npm install.

3. Once all the required modules are installed, execute the command - 'npm start' or `ng serve`
					
4. Application will be up and running and can be accessed through the url - http://localhost:4200/. Login to the applictaion using the default credentail (admin/admin).	
 
5. To Configure the server side application,please refer the readme file for the client project - https://manu_chandran@bitbucket.org/manu_chandran/travelservice.git


## Built With

* [Angular](https://angular.io/) - MVC Java Script framework
* [Node JS](https://nodejs.org/en/) -JS Runtime Environment
* [Bootstrap](https://getbootstrap.com/docs/4.1/getting-started/introduction/) - Responsive CSS Framework


## Author

* **Manu Chandran Shyamala** - (https://bitbucket.org/%7Ba8393a87-bbe7-4fbe-8ad8-1abece085926%7D/)


## License
