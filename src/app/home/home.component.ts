import { Component, OnInit } from '@angular/core';
import { CommonService } from '../common/common.service';

@Component({
  selector: 'home-component',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  private commonService:CommonService;
  public userName:String;

  constructor(commonService:CommonService) {
    this.commonService = commonService;
   }

  ngOnInit() {
    this.userName = this.commonService.userName;
  }

}
