import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { MarkentryComponent } from './markentry/markentry.component';
import { MarkexitComponent } from './markexit/markexit.component';
import { TravelhistoryComponent } from './travelhistory/travelhistory.component';
import { HomeComponent } from './home/home.component';
import { AuthGuard } from './login/auth-guard.service';



const routes: Routes = [
//  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path:'', component: HomeComponent,canActivate: [AuthGuard] },
  { path: 'home',  component: HomeComponent,canActivate: [AuthGuard]  },
  { path: 'login',  component: LoginComponent },
  { path: 'register', component: RegisterComponent,canActivate: [AuthGuard] },
  { path: 'markentry', component: MarkentryComponent,canActivate: [AuthGuard] },
  { path: 'markexit', component: MarkexitComponent,canActivate: [AuthGuard] },
  { path: 'travelhistory', component: TravelhistoryComponent,canActivate: [AuthGuard] },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}