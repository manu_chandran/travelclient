import { Component, OnInit } from '@angular/core';
import { LoginService } from './login.service';
import { LoginModel } from './login.model';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CommonService } from '../common/common.service';

@Component({
  selector: 'login-component',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  private username:string;
  private password:string;
  private loginService:LoginService;
  private loginStatus : string;
  private isLoginSuccess : boolean;
  private isLoginFailed : boolean;
  private loginModel: LoginModel;
  private router: Router;
  private commonService:CommonService;
  private redirectURL: string;
  private route: ActivatedRoute;

  constructor(loginService : LoginService,router: Router,commonService:CommonService,route: ActivatedRoute) {
    this.loginService = loginService;
    this.router =  router;
    this.loginStatus = '';
    this.commonService = commonService;
    this.route = route;
  }

  ngOnInit() {
    this.redirectURL = this.route.snapshot.queryParamMap.get('redirectURL');
  }

  onSubmit(){
    console.log('Form submitted successsfully');
    console.log('User Name  :'+this.username);
    console.log('Password  :'+ this.password);

    this.loginService.validateLoginUser(this.username,this.password).subscribe( 
      data => { 
        this.loginModel = data;
        console.log(this.loginModel);
        if(this.loginModel && this.loginModel.code==200){
          this.loginStatus = 'Login Successfull';
          localStorage.setItem('token', this.loginModel.token)
          console.log('this.loginModel.Code '+this.loginModel.code)
          this.commonService.userName = this.username;
          if(this.redirectURL){
            this.router.navigateByUrl(this.redirectURL);
          }else{
            this.router.navigateByUrl('/home');
          }
        }else{
          this.isLoginFailed = true;
          this.loginStatus = this.loginModel.status;
        }
      },
      err =>{ 
        console.log(err)
        this.loginStatus = 'Login Failed';
      }
      
    );   

  }


  logoutUser(){
    this.loginService.logout();
    this.router.navigateByUrl('/login');

  }


}
