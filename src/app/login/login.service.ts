import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {Observable} from 'rxjs/Rx';
import { LoginModel } from './login.model';
import { tokenNotExpired } from 'angular2-jwt';
import 'rxjs/add/operator/map';

@Injectable()
export class LoginService {

  private loginUrl = 'http://localhost:3000/rest/find/user'; 

  constructor(public http: Http) { 
  }

  validateLoginUser(userName : String, password : String):Observable<LoginModel>{
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    console.log('From LoginService,username : '+userName);
    console.log('From LoginService,password : '+password);
    let loginQueryUrl = this.loginUrl+'/username/'+userName+'/password/'+password; 
    return this.http.post(this.loginUrl,JSON.stringify({userName:userName,password:password}),{ headers: headers })
     .map((res:Response) => res.json())
     .catch((error:any) => Observable.throw(error));

  }

  logout() {
    localStorage.removeItem('token'); 
  }

  loggedIn():boolean {
    return tokenNotExpired();
  }
  
}
