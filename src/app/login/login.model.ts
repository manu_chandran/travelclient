export class LoginModel {

    public code:number;
    public status:string;
    public token:string;

    constructor(code, status,token) {
        this.code = code;
        this.status = status;
        this.token = token; 
    }

 

}