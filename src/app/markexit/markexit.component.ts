import { Component, OnInit } from '@angular/core';
import { MarkexitService } from './markexit.service';
import { ProfileSearchStatus } from '../register/profile.search.status.model';
import { DatePipe } from '@angular/common';
import { MarkExitStatus } from './markexit.status.model';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-markexit',
  templateUrl: './markexit.component.html',
  styleUrls: ['./markexit.component.css']
})
export class MarkexitComponent implements OnInit {

  private displayEntryDetails:boolean;
  private travelMarked:boolean;
  private passportNo:String;
  private nationality:String;
  private isInvalidPassportSearchParam:Boolean;
  private markexitService:MarkexitService;
  private profileSearchStatus:ProfileSearchStatus;
  private displayNoProfile:Boolean;
  private profileSearchError:Boolean;
  private timeOfEntry:Date;
  private datePipe: DatePipe
  private travelDestination:String;
  private flightCode:String;
  private markExitStatus:MarkExitStatus;
  private lastTravelExit: Boolean;
  private markExitError: Boolean;
  private isInvalidTravelAttributes:Boolean;

  private router: Router;

  constructor(markexitService:MarkexitService,datePipe: DatePipe,router:Router) {
    this.displayEntryDetails = false;
    this.travelMarked = false;
    this.markexitService = markexitService;
    this.datePipe = datePipe;
    this.router = router;
    this.router.routeReuseStrategy.shouldReuseRoute = function(){
      return false;
   }
   this.router.events.subscribe((evt) => {
      if (evt instanceof NavigationEnd) {
         this.router.navigated = false;
         window.scrollTo(0, 0);
      }
    });
   }

  ngOnInit() {
  }

  cleanUpVaidationErrors(){
    this.isInvalidPassportSearchParam = false;
    this.markExitError = false;
    this.displayNoProfile = false;
    this.travelMarked = false;
    this.lastTravelExit = false;
    this.isInvalidTravelAttributes = false;
  }


  initiateProfileSearch(){
    this.cleanUpVaidationErrors();
    console.log('Mark Entry Search submitted .......');
    if(!this.nationality || this.nationality == 'choose_nationality' || !this.passportNo){
      this.isInvalidPassportSearchParam = true;
      return;
    }
    this.markexitService.searchProfile(this.nationality,this.passportNo).subscribe( 
      data => { 
        this.profileSearchStatus = data;
        console.log(this.profileSearchStatus);
        if(this.profileSearchStatus && this.profileSearchStatus.code==200){
          this.displayEntryDetails =  true;
          console.log('Profile Searched Completed with one profile found.......... ');
          console.log(this.profileSearchStatus);
        }else{
          this.displayNoProfile = true;
          console.log('Profile Searched Completed with No profile found.......... ');
        }
      },
      err =>{ 
        if(err.status == 402 || err.status == 403){
          this.router.navigate(['/login'],{ queryParams: { 'redirectURL' :'markexit'}});
        }
        console.log(err)
        this.profileSearchError = true;
        console.log('Profile Search Failed............. ');
      }
      
    );
    //this.displayEntryDetails = true;
    this.travelMarked = false;
  }

  markExitTravel(){
    this.cleanUpVaidationErrors();
    console.log('Entry trvael mark triggred.......');
    if(!this.travelDestination || this.travelDestination == 'sourceOfTravel' || !this.flightCode || this.flightCode == 'choose_flight_code'){
      this.isInvalidTravelAttributes = true;
      return;
    }
    if(!this.timeOfEntry){
      this.timeOfEntry = new Date();
    }
    console.log(this.timeOfEntry);
    console.log(this.travelDestination);
    console.log(this.flightCode);
    this.markexitService.markExit(this.nationality,this.passportNo,this.travelDestination,
              this.flightCode,this.timeOfEntry).subscribe( 
      data => { 
        this.markExitStatus = data;
        console.log(this.profileSearchStatus);
        if(this.markExitStatus && this.markExitStatus.code==200){
          this.displayEntryDetails = false; 
          this.travelMarked = true;
          console.log('Mark Travel Successful.......... ');
          console.log(this.profileSearchStatus);
        }else if(this.markExitStatus && this.markExitStatus.code==205) {
          this.displayEntryDetails = false;
          this.lastTravelExit =  true;
          console.log('Mark Travel Failed,last travel was Entry.......... ');
        } else{
          this.displayEntryDetails = false;
          this.markExitError =  true;
          console.log('Mark Travel Failed with Code <> 200' );
        }
      },
      err =>{ 
        if(err.status == 402 || err.status == 403){
          this.router.navigate(['/login'],{ queryParams: { 'redirectURL' :'markexit'}});
        }
        console.log(err)
        this.markExitError = true;
        console.log('Mark Travel Failed,erorr Occured ', err);
      }
      
    );
  }

}
