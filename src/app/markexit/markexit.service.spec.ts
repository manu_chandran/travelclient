import { TestBed, inject } from '@angular/core/testing';

import { MarkexitService } from './markexit.service';

describe('MarkexitService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MarkexitService]
    });
  });

  it('should be created', inject([MarkexitService], (service: MarkexitService) => {
    expect(service).toBeTruthy();
  }));
});
