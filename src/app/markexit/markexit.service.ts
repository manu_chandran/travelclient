import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { ProfileSearchStatus } from '../register/profile.search.status.model';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { MarkExitStatus } from './markexit.status.model';
import { CommonService } from '../common/common.service';

@Injectable()
export class MarkexitService {

    private commonService:CommonService;
    private http:Http;
  
    constructor(http: Http,commonService:CommonService) { 
      this.http = http;
      this.commonService = commonService;
    }
  
    searchProfile(nationality:String,passportNo:String):Observable<ProfileSearchStatus>{
      console.log( 'Nationality : '+ nationality);
      console.log( 'passportNo : '+ passportNo);
      var requestParams = { 'passportNo' : passportNo, 'nationality' : nationality};
      var headers = this.commonService. getHeader();
      headers.append('Content-Type', 'application/json');
      return this.http.post(this.commonService.findProfileUrl,JSON.stringify(requestParams),{ headers: headers })
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error));
    }
  
  
    markExit(nationality:String,passportNo:String,toCountry:String,flightNo:String,
                    travelDate:Date):Observable<MarkExitStatus>{
      console.log( 'Nationality : '+ nationality);
      console.log( 'passportNo : '+ passportNo);
      var requestParams = { 'passportNo' : passportNo, 'nationality' : nationality,
      'toCountry' : toCountry,'flightNo' : flightNo,'travelDate':travelDate};
      var headers = this.commonService. getHeader();
      headers.append('Content-Type', 'application/json');
      return this.http.post(this.commonService.markExitUrl,JSON.stringify(requestParams),{ headers: headers })
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error));
    }

}
