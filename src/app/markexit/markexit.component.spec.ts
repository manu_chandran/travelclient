import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarkexitComponent } from './markexit.component';

describe('MarkexitComponent', () => {
  let component: MarkexitComponent;
  let fixture: ComponentFixture<MarkexitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarkexitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarkexitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
