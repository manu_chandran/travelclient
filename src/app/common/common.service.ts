import { Injectable } from "@angular/core";
import { Headers } from '@angular/http';

@Injectable()
export class CommonService{

    public travelHistoryUrl = 'http://localhost:3000/rest/find/travel'; 

    public findProfileUrl = 'http://localhost:3000/rest/find/profile'; 
  
    public markEntryUrl = 'http://localhost:3000/rest/mark/entry'; 
    
    public markExitUrl = 'http://localhost:3000/rest/mark/exit'; 

    public registerProfileUrl = 'http://localhost:3000/rest/create/profile'; 

    public userName: String;

    getHeader():Headers {
        var headers = new Headers();
        var tokenString = localStorage.getItem('token');
        if(tokenString){
            headers.append('token', tokenString);
        }
        return headers;
    }

    getHeaderString():string {
        return localStorage.getItem('token');
    }
}