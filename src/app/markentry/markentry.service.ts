import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { ProfileSearchStatus } from '../register/profile.search.status.model';
import { MarkEntryStatus } from './markentry.status.model';
import { CommonService } from '../common/common.service';

@Injectable()
export class MarkentryService {

  private commonService:CommonService;

  constructor(public http: Http,commonService:CommonService) { 
    this.commonService = commonService;
  }

  searchProfile(nationality:String,passportNo:String):Observable<ProfileSearchStatus>{
    console.log( 'Nationality : '+ nationality);
    console.log( 'passportNo : '+ passportNo);
    var requestParams = { 'passportNo' : passportNo, 'nationality' : nationality};
    var headers = this.commonService. getHeader();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.commonService.findProfileUrl,JSON.stringify(requestParams),{ headers: headers })
    .map((res:Response) => res.json())
    .catch((error:any) => Observable.throw(error));
  }


  markEntry(nationality:String,passportNo:String,fromCountry:String,flightNo:String,
                  travelDate:Date):Observable<MarkEntryStatus>{
    console.log( 'Nationality : '+ nationality);
    console.log( 'passportNo : '+ passportNo);
    var requestParams = { 'passportNo' : passportNo, 'nationality' : nationality,
    'fromCountry' : fromCountry,'flightNo' : flightNo,'travelDate':travelDate};
    var headers = this.commonService. getHeader();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.commonService.markEntryUrl,JSON.stringify(requestParams),{ headers: headers })
    .map((res:Response) => res.json())
    .catch((error:any) => Observable.throw(error));
  }

}
