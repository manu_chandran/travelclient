import { Component, OnInit } from '@angular/core';
import { MarkentryService } from './markentry.service';
import { ProfileSearchStatus } from '../register/profile.search.status.model';
import { DatePipe } from '@angular/common';
import { MarkEntryStatus } from './markentry.status.model';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'markentry-component',
  templateUrl: './markentry.component.html',
  styleUrls: ['./markentry.component.css']
})
export class MarkentryComponent implements OnInit {

  private displayEntryDetails:boolean;
  private travelMarked:boolean;
  private passportNo:String;
  private nationality:String;
  private isInvalidPassportSearchParam:Boolean;
  private markentryService:MarkentryService;
  private profileSearchStatus:ProfileSearchStatus;
  private displayNoProfile:Boolean;
  private profileSearchError:Boolean;
  private timeOfEntry:Date;
  private datePipe: DatePipe
  private sourceOfTravel:String;
  private flightCode:String;
  private markEntryStatus:MarkEntryStatus;
  private lastTravelEntry: Boolean;
  private markEntryError: Boolean;
  private isInvalidTravelAttributes:Boolean;

  private router: Router;

  constructor(markentryService:MarkentryService,datePipe: DatePipe,router: Router) { 
    this.displayEntryDetails = false;
    this.travelMarked = false;
    this.markentryService = markentryService;
    this.datePipe = datePipe;
    this.router =  router;

    this.router.routeReuseStrategy.shouldReuseRoute = function(){
      return false;
    }
    this.router.events.subscribe((evt) => {
      if (evt instanceof NavigationEnd) {
         this.router.navigated = false;
         window.scrollTo(0, 0);
      }
    });
  }

  ngOnInit() {
  }

  cleanUpVaidationErrors(){
    this.isInvalidPassportSearchParam = false;
    this.markEntryError = false;
    this.displayNoProfile = false;
    this.travelMarked = false;
    this.lastTravelEntry = false;
    this.isInvalidTravelAttributes = false;
  }

  initiateProfileSearch(){
    this.cleanUpVaidationErrors();
    console.log('Mark Entry Search submitted .......');
    if(!this.nationality || this.nationality == 'choose_nationality' || !this.passportNo){
      this.isInvalidPassportSearchParam = true;
      return;
    }
    this.markentryService.searchProfile(this.nationality,this.passportNo).subscribe( 
      data => { 
        this.profileSearchStatus = data;
        console.log(this.profileSearchStatus);
        if(this.profileSearchStatus && this.profileSearchStatus.code==200){
          this.displayEntryDetails =  true;
          console.log('Profile Searched Completed with one profile found.......... ');
          console.log(this.profileSearchStatus);
        }else{
          this.displayNoProfile = true;
          console.log('Profile Searched Completed with No profile found.......... ');
        }
      },
      err =>{ 
        if(err.status == 402 || err.status == 403){
          this.router.navigate(['/login'],{ queryParams: { 'redirectURL' :'markentry'}});
        }
        console.log(err)
        this.profileSearchError = true;
        console.log('Profile Search Failed............. ');
      }
      
    );
    //this.displayEntryDetails = true;
    this.travelMarked = false;
  }

  markEntryTravel(){
    this.cleanUpVaidationErrors();
    console.log('Entry trvael mark triggred.......');
    if(!this.sourceOfTravel || this.sourceOfTravel == 'sourceOfTravel' || !this.flightCode || this.flightCode == 'choose_flight_code'){
      this.isInvalidTravelAttributes = true;
      return;
    }
    if(!this.timeOfEntry){
      this.timeOfEntry = new Date();
    }
    console.log(this.timeOfEntry);
    console.log(this.sourceOfTravel);
    console.log(this.flightCode);
    this.markentryService.markEntry(this.nationality,this.passportNo,this.sourceOfTravel,
              this.flightCode,this.timeOfEntry).subscribe( 
      data => { 
        this.markEntryStatus = data;
        console.log(this.profileSearchStatus);
        if(this.markEntryStatus && this.markEntryStatus.code==200){
          this.displayEntryDetails = false; 
          this.travelMarked = true;
          console.log('Mark Travel Successful.......... ');
          console.log(this.profileSearchStatus);
        }else if(this.markEntryStatus && this.markEntryStatus.code==205) {
          this.displayEntryDetails = false;
          this.lastTravelEntry =  true;
          console.log('Mark Travel Failed,last travel was Entry.......... ');
        } else{
          this.displayEntryDetails = false;
          this.markEntryError =  true;
          console.log('Mark Travel Failed with Code <> 200' );
        }
      },
      err =>{ 
        if(err.status == 402 || err.status == 403){
          this.router.navigate(['/login'],{ queryParams: { 'redirectURL' :'markentry'}});
        }
        console.log(err)
        this.markEntryError = true;
        console.log('Mark Travel Failed,erorr Occured ', err);
      }
      
    );

  }
}