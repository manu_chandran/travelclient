import { TestBed, inject } from '@angular/core/testing';

import { MarkentryService } from './markentry.service';

describe('MarkentryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MarkentryService]
    });
  });

  it('should be created', inject([MarkentryService], (service: MarkentryService) => {
    expect(service).toBeTruthy();
  }));
});
