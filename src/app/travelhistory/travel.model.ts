export class Travel {
    public passportno:String;
    public nationality:String;
    public traveltype:String;
    public traveldate:Date;
    public fromcountry:String;
    public tocountry:String;
    public cariertype:String;
    public flightno:String; 
    public permitno: String;
    public remarks: String;

    constructor(passportno,nationality,traveltype,traveldate,fromcountry,tocountry,cariertype,
    flightno,permitno,remarks){
        this.passportno = passportno;
        this.nationality = nationality;
        this.traveltype =  traveltype;
        this.traveldate = traveldate;
        this.fromcountry = fromcountry;
        this.tocountry = tocountry;
        this.cariertype = cariertype;
        this.flightno =  flightno;
        this.permitno = permitno;
        this.remarks = remarks;
    }


}

