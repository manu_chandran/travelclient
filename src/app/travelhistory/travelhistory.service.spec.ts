import { TestBed, inject } from '@angular/core/testing';

import { TravelhistoryService } from './travelhistory.service';

describe('TravelhistoryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TravelhistoryService]
    });
  });

  it('should be created', inject([TravelhistoryService], (service: TravelhistoryService) => {
    expect(service).toBeTruthy();
  }));
});
