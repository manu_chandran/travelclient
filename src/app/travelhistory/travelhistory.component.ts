import { Component, OnInit } from '@angular/core';
import { TravelhistoryService } from './travelhistory.service';
import { TravelDetail } from './traveldetail';
import { TravelHistoryStatus } from './travel.history.status.model';
import { Travel } from './travel.model';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-travelhistory',
  templateUrl: './travelhistory.component.html',
  styleUrls: ['./travelhistory.component.css']
})
export class TravelhistoryComponent implements OnInit {

  private travelhistoryService : TravelhistoryService;
  private nationality:string;
  private passportNo:string;
  private displayTravelDetails:boolean;
  private noTravelFound:boolean;
  private travelResponseError:boolean;
  private router: Router;
  private isInvalidPassportSearchParam:Boolean;
  
  private travelHistoryDetails:TravelHistoryStatus;

  constructor(travelhistoryService:TravelhistoryService,router: Router) {
     this.travelhistoryService = travelhistoryService;
     this.router = router;

     this.router.routeReuseStrategy.shouldReuseRoute = function(){
      return false;
    }
    this.router.events.subscribe((evt) => {
      if (evt instanceof NavigationEnd) {
         this.router.navigated = false;
         window.scrollTo(0, 0);
      }
    });
   }

  ngOnInit() {
  }

  cleanUpVaidationErrors(){
    this.travelResponseError = false;
    this.displayTravelDetails = false;
    this.noTravelFound = false;
    this.isInvalidPassportSearchParam = false;
  }

  fetchTravelDetails(){
    console.log('The Nationality : '+this.nationality);
    console.log('The Passport No : '+this.passportNo);
    if(!this.nationality || this.nationality == 'choose_nationality' || !this.passportNo){
      this.isInvalidPassportSearchParam = true;
      return;
    }
    this.cleanUpVaidationErrors();
    this.travelhistoryService.fetchTravelDetails(this.nationality,this.passportNo).subscribe( 
      data => { 
        this.travelHistoryDetails = data;
        console.log(this.travelHistoryDetails);
        if(this.travelHistoryDetails && this.travelHistoryDetails.code==200 && this.travelHistoryDetails.traveldetails.length > 0){
          this.displayTravelDetails = true;
          console.log('Mark Travel Successful.......... ');
        } else{
          this.noTravelFound = true;
          console.log('Mark Travel Failed with Code <> 200' );
        }
      },
      err =>{ 
        if(err.status == 402 || err.status == 403){
          this.router.navigate(['/login'],{ queryParams: { 'redirectURL' :'travelhistory'}});
        }

        console.log(err)
        this.travelResponseError = true;
        console.log('Mark Travel Failed,erorr Occured ', err);
      }
      
    );
  }

}
