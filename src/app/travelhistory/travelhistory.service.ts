import { Injectable } from '@angular/core';
import { TravelHistoryStatus } from './travel.history.status.model';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { ProfileSearchStatus } from '../register/profile.search.status.model';
import { AuthHttp, tokenNotExpired } from 'angular2-jwt';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { CommonService } from '../common/common.service';

@Injectable()
export class TravelhistoryService {

  private http:Http;
  private travelHistoryStatus:TravelHistoryStatus
  private commonService:CommonService;

  constructor(http: Http,commonService:CommonService) { 
    this.http = http;
    this.commonService = commonService
  }

  fetchTravelDetails(nationality:String,passportNo:String):Observable<TravelHistoryStatus>{

    console.log( 'Nationality : '+ nationality);
    console.log( 'passportNo : '+ passportNo);
    //var header = this.commonService.getHeaderString();
    var requestParams = { 'nationality' : nationality,'passportNo' : passportNo};
    let headers = this.commonService.getHeader();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.commonService.travelHistoryUrl,JSON.stringify(requestParams), options )
    .map((res:Response) => res.json())
    .catch((error:any) => Observable.throw(error));
  }


}
