import { Injectable } from '@angular/core';
import { Register } from './register.model';
import { RegisterStatus } from './register.status.model';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { CommonService } from '../common/common.service';

@Injectable()
export class RegisterService {

  private http:Http;
  private commonService:CommonService;

  constructor(http: Http,commonService:CommonService) { 
    this.http = http;
    this.commonService =commonService;
  }

  registerProfile(register:Register):Observable<RegisterStatus>{
    console.log( register);
    var headers = this.commonService.getHeader();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.commonService.registerProfileUrl,JSON.stringify(register),{ headers: headers })
    .map((res:Response) => res.json())
    .catch((error:any) => Observable.throw(error));
  }

}
