import { Register } from "./register.model";
import { Travel } from "../travelhistory/travel.model";
import { Permit } from "./permit.model";

export class ProfileSearchStatus {

    public code : number;
    public status : String;
    public profile : Register;
    public travel : Travel;
    public permit : Permit;


}