export class Register {
    
    public firstName:string;
    public middleName:string;
    public lastName:string;
    public nationality:string;
    public passportNo:string;
    public gender:string;
    public dateOfBirth:Date;
    public profession:string;
    public issueCountry:string;
    public issueGov:string;
    public passportExpiryDate:Date;
    public passportCreatedDate:Date;





    constructor(firstname,middlename,lastname,nationality,passportNo,gender,
        dateOfBirth,profession,issueCountry,issueGov,passportExpiryDate,passportCreatedDate) { 
        this.firstName = firstname;
        this.middleName = middlename;
        this.lastName = lastname;
        this.nationality = nationality;
        this.passportNo = passportNo;
        this.gender = gender;
        this.dateOfBirth = dateOfBirth;
        this.profession = profession;
        this.issueCountry = issueCountry;
        this.issueGov = issueGov;
        this.passportExpiryDate = passportExpiryDate;    
        this.passportCreatedDate = passportCreatedDate;
    }


}