export class Permit{

    public permitno:String;
    public visatype:String;
    public permitstatus:String;
    public createddate:Date;
    public expirydate:Date;  
    public establishment: String;
    public passportno:String;
    public nationality:String;

    constructor(permitno,visatype,permitstatus,createddate,expirydate,establishment,passportno,nationality){
        this.permitno = permitno;
        this.visatype = visatype;
        this.permitstatus = permitstatus;
        this.createddate = createddate;
        this.expirydate = expirydate;
        this.establishment =  establishment;
        this.passportno = passportno;
        this.nationality = nationality;
    }



}