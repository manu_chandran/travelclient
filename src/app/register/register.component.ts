import { RegisterStatus } from './register.status.model';
import { RegisterService } from './register.service';
import { Component, OnInit } from '@angular/core';
import { Register } from './register.model';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'register-component',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  private firstName:string;
  private middleName:string;
  private lastName:string;
  private gender:string;
  private dateOfBirth:Date;
  private profession:string;
  private issueCountry:string;
  private issueGov:string;
  private passportExpiryDate:Date;
  private passportCreatedDate:Date;
  private nationality:string;
  private passportNo:string;

  private registerService:RegisterService;
  private register:Register;
  private registerStatus : RegisterStatus;
  private isProfileCreatedSucessfully : Boolean;
  private isProfileCreationFailed : Boolean;
  private showProfileCreationForm : Boolean;

  private router: Router;

  constructor(registerService:RegisterService,router: Router) {
    this.registerService =  registerService;
    this.showProfileCreationForm = true;
    this.isProfileCreatedSucessfully = false;
    this.isProfileCreationFailed = false;
    this.router = router;

    this.router.routeReuseStrategy.shouldReuseRoute = function(){
      return false;
    }
    this.router.events.subscribe((evt) => {
        if (evt instanceof NavigationEnd) {
          this.router.navigated = false;
          window.scrollTo(0, 0);
        }
      });
   }

  ngOnInit() {
  }

  onSubmit(){    
    this.isProfileCreatedSucessfully = false;
    this.isProfileCreationFailed = false;
    console.log('Register Form Submitted......');
    this.register = new Register(this.firstName,this.middleName,this.lastName,this.nationality,this.passportNo,
      this.gender,this.dateOfBirth,this.profession,this.issueCountry,this.issueGov,this.passportExpiryDate,
        this.passportCreatedDate);
    this.registerService.registerProfile(this.register).subscribe( 
      data => { 
        this.registerStatus = data;
        console.log(this.registerStatus);
        if(this.registerStatus && this.registerStatus.code==200){
          this.isProfileCreatedSucessfully = true;
          this.showProfileCreationForm = false;
        }else{
          this.isProfileCreationFailed = true;
        }
        console.log('Profile Creation Status............. '+this.registerStatus);
      },
      err =>{ 
        if(err.status == 402 || err.status == 403){
          this.router.navigate(['/login'],{ queryParams: { 'redirectURL' :'register'}});
        }
        console.log(err)
        this.isProfileCreatedSucessfully = false;
        console.log('Profile Creation Failed............. '+this.registerStatus)
      }
      
    );
  }

}
